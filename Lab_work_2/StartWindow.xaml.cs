﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lab_work_2
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// </summary>
    
    public partial class StartWindow : Window
    {
        public static bool x;
        public StartWindow()
        {
            InitializeComponent();
        }

      
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            MessageBox.Show("Спасибо!");
            x = true;
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
            x = false;
        }
    }
}

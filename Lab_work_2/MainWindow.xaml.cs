﻿/*
 Filonenko_V 19.04.2019
 Я программа, которая будет выполнять функции автоматического парсера информации из официального банка данных угроз ФСТЭК России

 В качестве БД использована Excel
 Обновление всех сведений локальной базы данных угроз безопасности информации в файле на жестком диске компьютера (автоматическое при каждом обновлении) 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;


namespace Lab_work_2
{
    
    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            
            InitializeComponent();
            Start_();
        }


        static string startPathFile = System.IO.Path.GetFullPath(@"..\..\");
        static string pathFile = startPathFile + @"AddData\\thrlist.xlsx";
        static List<threat> threats = new List<threat>();
        
        static int left_threat;
        static int rigth_threat;
        static int max_id_threat;



        private void Start_()
        {
            if (!File.Exists(pathFile))
            {
                StartWindow startWindow = new StartWindow();
                startWindow.ShowDialog();
                if (StartWindow.x == true)
                    Environment.Exit(0);
                else
                {
                    DownloadFile();
                    MessageBox.Show("Файл успешно загружен.");
                }

                
            }
            Update update = new Update();
            update.Show();
            ExcelRead(pathFile, threats);
            update.Close();
            left_threat = 0;
            rigth_threat = 19;
            ItemSoureThreats(threats, left_threat, rigth_threat);


        }
        private void DownloadFile() //  Загрузка файла
        {
            try
            {
                WebClient webClient = new WebClient();
                webClient.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", pathFile);
                
            }
            catch (WebException)
            {
                MessageBox.Show("Ошибка скачивания");
                Environment.Exit(0);
            }
        }
        private void Button_Click_After(object sender, RoutedEventArgs e) // Показать информацию до обновления
        {

            BorderThreats.Visibility = Visibility.Hidden;
            BorderThreatsUpdate.Visibility = Visibility.Visible;
        }
        private void Button_Click_Before(object sender, RoutedEventArgs e) // Показать информацию после обновления
        {
            BorderThreatsUpdate.Visibility = Visibility.Hidden;
            BorderThreats.Visibility = Visibility.Visible;
        }
        private void ItemSoureThreats(List<threat> threats, int left, int rigth) // Вывод 20 элементов 
        {
            if (max_id_threat - rigth < 0)
            {
                while (max_id_threat - rigth <= 0)
                    rigth--;
                
            }
            List<threat> scrollThreats = new List<threat>();
            for (int j = left; j < rigth + 1; j++)
                scrollThreats.Add(threats[j]);
            lstThreats.ItemsSource = scrollThreats;
            
        }
        
        private void ExcelRead(string pathFile, List<threat> threats) // Чтение Excel
        {

            Excel.Application ex = new Excel.Application();
            ex.Visible = false;
            ex.SheetsInNewWorkbook = 12;
            Excel.Workbook workBook = ex.Workbooks.Open(pathFile,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing);


            Excel.Worksheet worksheet = (Excel.Worksheet)workBook.Sheets[1];



            max_id_threat = worksheet.UsedRange.Rows.Count - 2;
            string s1 = "нет", s2 = "нет", s3 = "нет";
            for (int i = 3; i < worksheet.UsedRange.Rows.Count + 1; i++)
            {
                if ((int)worksheet.Cells[i, 6].Value == 1)
                    s1 = "да";
                if ((int)worksheet.Cells[i, 7].Value == 1)
                    s2 = "да";
                if ((int)worksheet.Cells[i, 8].Value == 1)
                    s3 = "да";
                threats.Add(new threat
                {
                    Threat_identifier = (int)worksheet.Cells[i, 1].Value,
                    Threat_name = worksheet.Cells[i, 2].Value,
                    Description_of_the_threat = worksheet.Cells[i, 3].Value,
                    Source_of_threat = worksheet.Cells[i, 4].Value,
                    Object_of_the_impact_of_threats = worksheet.Cells[i, 5].Value,
                    Privacy_violation = s1,
                    Integrity_violation = s2,
                    Accessibility_violation = s3,
                    Data = worksheet.Cells[i, 10].Value ,
                    Threat_identifier_update = (int)worksheet.Cells[i, 1].Value,
                    Threat_name_update = worksheet.Cells[i, 2].Value,
                    Description_of_the_threat_update = worksheet.Cells[i, 3].Value,
                    Source_of_threat_update = worksheet.Cells[i, 4].Value,
                    Object_of_the_impact_of_threats_update = worksheet.Cells[i, 5].Value,
                    Privacy_violation_update = s1,
                    Integrity_violation_update = s2,
                    Accessibility_violation_update = s3,
                    Data_update = worksheet.Cells[i, 10].Value
                });
            }
            workBook.Close();
            ex.Quit();
            // Здесь нужен костюль; Оказывается, Exsel просто так не закрыть :c
            var process = Process.GetProcessesByName("EXCEL");
            process[0].Kill();
        }



        private void ExcelUpdate() // Обновление данных ( Не учитывал создание и удаление записей )
        {
            Excel.Application ex = new Excel.Application();
            ex.Visible = false;
            ex.SheetsInNewWorkbook = 12;
            Excel.Workbook workBook = ex.Workbooks.Open(pathFile,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing);
            Excel.Worksheet worksheet = (Excel.Worksheet)workBook.Sheets[1];
            
            try
            {
                string s1 = "нет", s2 = "нет", s3 = "нет";
                for (int i = 3; i < worksheet.UsedRange.Rows.Count + 1; i++)
                {
                  
                    threats[i - 3].Threat_identifier = threats[i - 3].Threat_identifier_update;                   
                    threats[i - 3].Threat_name = threats[i - 3].Threat_name_update;
                    threats[i - 3].Description_of_the_threat = threats[i - 3].Description_of_the_threat_update;
                    threats[i - 3].Source_of_threat = threats[i - 3].Source_of_threat_update;
                    threats[i - 3].Object_of_the_impact_of_threats = threats[i - 3].Object_of_the_impact_of_threats_update;
                    threats[i - 3].Privacy_violation = threats[i - 3].Privacy_violation_update;
                    threats[i - 3].Integrity_violation = threats[i - 3].Integrity_violation_update;
                    threats[i - 3].Accessibility_violation = threats[i - 3].Accessibility_violation_update;
                    threats[i - 3].Data = threats[i - 3].Data_update;

                    if ((int)worksheet.Cells[i, 6].Value == 1)
                        s1 = "да";
                    if ((int)worksheet.Cells[i, 7].Value == 1)
                        s2 = "да";
                    if ((int)worksheet.Cells[i, 8].Value == 1)
                        s3 = "да";
                    threats[i - 3].Threat_identifier_update = (int)worksheet.Cells[i, 1].Value;
                    if (threats[i - 3].Data != worksheet.Cells[i, 10].Value)
                    { threats[i - 3].Threat_name += " (Обновлено)"; }
                    threats[i - 3].Threat_name_update = worksheet.Cells[i, 2].Value;
                    threats[i - 3].Description_of_the_threat_update = worksheet.Cells[i, 3].Value;
                    threats[i - 3].Source_of_threat_update = worksheet.Cells[i, 4].Value;
                    threats[i - 3].Object_of_the_impact_of_threats_update = worksheet.Cells[i, 5].Value;
                    threats[i - 3].Privacy_violation_update = s1;
                    threats[i - 3].Integrity_violation_update = s2;
                    threats[i - 3].Accessibility_violation_update = s3;
                    threats[i - 3].Data_update = worksheet.Cells[i, 10].Value;


                }
            }
            finally
            {
                workBook.Close();
                ex.Quit();
                // Здесь нужен костюль; (всё также грустно) 
                var process = Process.GetProcessesByName("EXCEL");
                process[0].Kill();
            }
        }



        private void CmdUpdateThreats_Click(object sender, RoutedEventArgs e)
        {
            Update update = new Update();
            update.Show();
            DownloadFile();
            try
            {
                ExcelUpdate();
                AfterButton.IsEnabled = true;
                BeforeButton.IsEnabled = true;
                ItemSoureThreats(threats, left_threat, rigth_threat);

            }
            catch
            {
                MessageBox.Show("Ошибка чтения");
            }
            finally
            {               
                update.Close();
            }
            

        }

        private void Button_Rigth(object sender, RoutedEventArgs e) // Следующие 20 элементов при их существовании   => 
        {
            if (max_id_threat > rigth_threat)
            {
                left_threat += 20;
                rigth_threat += 20;
            }
            
            ItemSoureThreats(threats, left_threat, rigth_threat);
        }

        private void Button_Left(object sender, RoutedEventArgs e) // Следующие 20 элементов при их существовании   <=
        {
            if (left_threat >=20)
            {
                left_threat -= 20;
                rigth_threat -= 20;
            }
            
            ItemSoureThreats(threats, left_threat, rigth_threat);
        }

        
    }
   
}
    
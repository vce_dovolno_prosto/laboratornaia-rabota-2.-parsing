﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_work_2
{
    class threat
    {
        public int Threat_identifier { set; get; }
        public string Threat_name { set; get; }
        public string Description_of_the_threat { set; get; }
        public string Source_of_threat { set; get; }
        public string Object_of_the_impact_of_threats { set; get; }
        public string Privacy_violation { set; get; }
        public string Integrity_violation { set; get; }
        public string Accessibility_violation { set; get; }
        public DateTime Data { set; get; }

        public override string ToString()
        {
            return Threat_identifier + ". " + Threat_name;
        }

        public int Threat_identifier_update { set; get; }
        public string Threat_name_update { set; get; }
        public string Description_of_the_threat_update { set; get; }
        public string Source_of_threat_update { set; get; }
        public string Object_of_the_impact_of_threats_update { set; get; }
        public string Privacy_violation_update { set; get; }
        public string Integrity_violation_update { set; get; }
        public string Accessibility_violation_update { set; get; }
        public DateTime Data_update { set; get; }
    }

    
}
